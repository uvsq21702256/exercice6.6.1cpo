package uvsq.Exercice5.CPO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.events.EventException;

public class DnsTUI {
	
    String IPADDRESS_PATTERN ="(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    String NOMMACHINE_PATTERN = "^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$";
    String DOMAINElsa_PATTERN ="ls -a [a-zA-Z0-9}=]+";
    String DOMAINEls_PATTERN ="ls [a-zA-Z0-9}=]+";
    //String EXIT_PATTERN ="exit|cd|end";
    DnsItem dnsitem= new DnsItem(new AdresseIP(0,0,0,0), new NomMachine("", ""));
    
   public ArrayList<DnsItem> lists = new ArrayList<DnsItem>();
	
    
public Commande  NextCommande(String saisie) throws IOException
{
        lists.clear();
	    String s=saisie;
   

        if(s!=null)
        {    
        	
        	// CHANGEMENT NEW CLASS FOR EACH EXECUTE COPIER COLLER THESE METHODES          
        	Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
		    Matcher matcher = pattern.matcher(s);
		    if (matcher.find()) { 
		    	//  Adresse IP
		    String[] parts = s.split("\\.");
		    int adr =Integer.parseInt(parts[0]); 
		    int es =Integer.parseInt(parts[1]);
		    int se=Integer.parseInt(parts[2]);
		    int ip =Integer.parseInt(parts[3]); 
		    int[] results = new int[parts.length];
		 
		 
		  AdresseIP  ipp= new AdresseIP(adr,es,se,ip);
	
	     InterfaceUserRechIP dns= new InterfaceUserRechIP(ipp);
		
		try {
			lists.addAll(dns.execute());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dns;
	
		    
		    } else{
		    	
		    	pattern = Pattern.compile(NOMMACHINE_PATTERN);
			     matcher = pattern.matcher(s);
			     if (matcher.find()) {  
				 String[] parts = s.split("\\.");
			     NomMachine nom= new NomMachine(parts[0],parts[1]);
				 InterfaceUserRechMachine dns= new InterfaceUserRechMachine(nom);
				 try {
					lists.addAll(dns.execute());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 return dns;
				
				    
				    } else{ 
				    	
				    	// on teste avec getItemS
				    	
				    	pattern = Pattern.compile(DOMAINElsa_PATTERN);
					     matcher = pattern.matcher(s);
					     
					     if (matcher.find()) {   System.out.println("C'est un ls -a ");
						    
						    
						    // On fait appel a getItems(NomMachine.Domaine)
					     
					     
					     String[] parts = s.split("ls -a ");
					    
					    
					     NomMachine nom= new NomMachine(parts[1]);
					     
					    InterfaceUserRechNom dns= new InterfaceUserRechNom(nom);
						
						lists.addAll(dns.execute());
					     
						Collections.sort(lists, new Comparator<DnsItem>() {
						    @Override
						    public int compare(DnsItem o1, DnsItem o2) {
						       if ( o1.AIP.ConcateIP()> o2.AIP.ConcateIP()) return -1;
						       else if ( o2.AIP.ConcateIP()> o1.AIP.ConcateIP()) return 1;
						       else return 0;
						    }
						});

						    
						    } else{ 
						    	// teste si ls  et le TRI + les Testes
						    	 
						    	 pattern = Pattern.compile(DOMAINEls_PATTERN);
							     matcher = pattern.matcher(s);
							     
							     				if (matcher.find()) {   
							     
								    
							     				String[] parts = s.split("ls ");
											    
											    
											     NomMachine nom= new NomMachine(parts[1]);
											     
											    InterfaceUserRechNom dns= new InterfaceUserRechNom(nom);
												
												lists.addAll(dns.execute());
												
												
												Collections.sort(lists, new Comparator<DnsItem>() {
												    @Override
												    public int compare(DnsItem o1, DnsItem o2) {
												    	
												    	
												        return o1.Machine.Nom.compareTo(o2.Machine.Nom);
												    }
												});

								    
							     								}
							     
							     
							     				else{ 
							     					
							     					{
							     						
								     					System.out.println(" FAUTE SYNTAXIQUE ");}
							     
							     
								    }
						    	
						    	
						    }
				    	
				    	
				    	
				    }
				    }
		       
		        
		     
		        
		    }
        
        
        return   null;
}
        



public void Afficher()
{try{   
				
		for (int i=0; i<lists.size(); i++) {
		System.out.println(" machine.domaine "+lists.get(i).Machine.Nom+"."+lists.get(i).Machine.Domaine+" IP: "+lists.get(i).AIP.adr+"."+lists.get(i).AIP.es+"."+lists.get(i).AIP.se+"."+lists.get(i).AIP.ip);
					}
	 }
		catch(EventException f){
		System.out.println(" Adresse IP INEXISTANTE ! ");
	}

}









}

